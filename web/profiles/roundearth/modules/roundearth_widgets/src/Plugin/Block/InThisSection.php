<?php

namespace Drupal\roundearth_widgets\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'In this section' block.
 *
 * @Block(
 *   id = "in_this_section_block",
 *   admin_label = @Translation("In this section"),
 *   category = @Translation("Roundearth")
 * )
 */
class InThisSection extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * InThisSection constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $storage, MenuLinkTreeInterface $menu_tree) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuTree = $menu_tree;
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('menu'),
      $container->get('menu.link_tree')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $menus = $this->storage->loadMultiple();

    $options = [];
    /** @var \Drupal\system\MenuInterface $menu */
    foreach ($menus as $menu) {
      $options[$menu->id()] = $menu->label();
    }

    $form['menu_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu'),
      '#description' => $this->t('The menu to use for building the child menu links.'),
      '#options' => $options,
      '#default_value' => $config['menu_name'],
      '#required' => TRUE,
    ];

    $form['suggestion'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Theme hook suggestion'),
      '#default_value' => $config['suggestion'],
      '#field_prefix' => '<code>menu__</code>',
      '#description' => $this->t('A theme hook suggestion can be used to override the default HTML and CSS classes for menus found in <code>menu.html.twig</code>.'),
      '#machine_name' => [
        'error' => $this->t('The theme hook suggestion must contain only lowercase letters, numbers, and underscores.'),
        'exists' => [$this, 'suggestionExists'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['menu_name'] = $form_state->getValue('menu_name');
    $this->configuration['suggestion'] = $form_state->getValue('suggestion');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'menu_name' => 'main',
      'suggestion' => 'breadcrumb',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $menu_name = $this->configuration['menu_name'];
    $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters($menu_name);

    // Get the starting level.
    $menu_trail_ids = array_values($parameters->activeTrail);

    // Return if we are the top root menu.
    if (count($menu_trail_ids) == 1 && ($menu_trail_ids[0] == '')) {
      return [];
    }

    $menu_root = reset($menu_trail_ids);

    // Set the root and the depths.
    $parameters->setRoot($menu_root)->setMinDepth(1)->setMaxDepth(1);

    // We don't want any expanded parents.
    $parameters->expandedParents = [];

    // Build the menu tree.
    $tree = $this->menuTree->load($menu_name, $parameters);

    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];

    $tree = $this->menuTree->transform($tree, $manipulators);
    $tree = $this->menuTree->build($tree);

    if (!isset($tree['#items'])) {
      return [];
    }

    // Add theme suggestion.
    if ($this->configuration['suggestion']) {
      $tree['#theme'] = 'menu__' . $this->configuration['suggestion'];
    }

    return $tree;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $cache_contexts = parent::getCacheContexts();
    $cache_contexts[] = 'route.menu_active_trails:' . $this->configuration['menu_name'];
    $cache_contexts[] = 'url.path';
    return $cache_contexts;
  }

  /**
   * Checks for an existing theme hook suggestion.
   *
   * @return bool
   *   Returns FALSE because there is no need of validation by unique value.
   */
  public function suggestionExists() {
    return FALSE;
  }
}
