<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Class Equals.
 *
 * Determine if the value is equal to a specified value.
 *
 * Options:
 *
 * - value: The value to check against. May be a singular value, or an array
 *     where all values are checked against.
 * - true_value: The value returned when the value is equal.
 * - false_value: The value returned when the value is not equal.
 * - negate: Negate the check, skip when not equals (not implemented).
 * - strict: Use strict comparator (not implemented).
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_equals"
 * )
 */
class Equals extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($this->equals($value)) {
      $value = isset($this->configuration['true_value']) ? $this->configuration['true_value'] : TRUE;
    }
    else {
      $value = isset($this->configuration['false_value']) ? $this->configuration['false_value'] : FALSE;
    }

    return $value;
  }

  /**
   * See if a value equals another value.
   *
   * @param mixed $value
   *   The value to check for equalness.
   *
   * @return bool
   *   Indicates if the value is equal.
   */
  protected function equals($value) {
    if (is_array($this->configuration['value'])) {
      return in_array($value, $this->configuration['value']);
    }

    return $value == $this->configuration['value'];
  }

}
