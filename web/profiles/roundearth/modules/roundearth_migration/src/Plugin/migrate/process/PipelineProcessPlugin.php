<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Pipeline process plugin.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_pipeline"
 * )
 */
class PipelineProcessPlugin extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($this->configuration['pipeline'])) {
      return $value;
    }

    if (!$pipeline = $this->getPipeline($this->configuration['pipeline'])) {
      return $value;
    }

    return $pipeline->process($value, $migrate_executable, $row, $destination_property);
  }

  /**
   * Gets a pipeline processor.
   *
   * @param string $id
   *   The ID of the pipeline processor to load.
   *
   * @return \Drupal\roundearth_migration\Entity\PipelineProcessor
   *   The pipeline processor.
   */
  protected function getPipeline($id) {
    return \Drupal::entityTypeManager()
      ->getStorage('pipeline_processor')
      ->load($id);
  }

}
