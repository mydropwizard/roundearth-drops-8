<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use DOMDocument;
use DOMElement;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UrlBasePath.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_url_base_path"
 * )
 */
class UrlBasePath extends UrlProcessorBase implements ContainerFactoryPluginInterface {

  /**
   * Default configuration values.
   *
   * @var array
   */
  protected $defaultConfig = [
    'base_path' => '/',
    'files_path' => 'sites/default/files',
    'hosts' => [],
    'source_base_path_map' => []
  ];

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * UrlBasePath constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImmutableConfig $settings) {
    $this->settings = $settings;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('roundearth_migration.settings')
    );
  }

  /**
   * Get plugin configuration.
   *
   * @return array
   *   Configuration values.
   */
  protected function getConfig() {
    $config = [];
    foreach ($this->defaultConfig as $key => $value) {
      $val = $this->settings->get($key);
      $config[$key] = $val !== NULL ? $val : $this->defaultConfig[$key];
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  protected function processUrl($url) {
    if (!$url = $this->internalUrl($url)) {
      return FALSE;
    }

    $conf = $this->getConfig();

    // Replace some stuff!
    foreach ($conf['source_base_path_map'] as $src => $dest) {
      $src = $this->normalizePathSlashes($src);
      $dest = $this->normalizePathSlashes($dest);
      $srcLen = strlen($src);

      if (substr($url, 0, $srcLen) == $src) {
        $url = $dest . substr($url, $srcLen);
      }
    }

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  protected function setUrl(DOMElement $element, $attribute, $value, $original) {
    parent::setUrl($element, $attribute, $value, $original);
    switch ($element->tagName) {
      case 'a':
        $message = sprintf('Link with href="%s" updated to "%s".', $original, $value);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        break;

      case 'img':
        $message = sprintf('Image with src="%s" updated to "%s".', $original, $value);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function deferSetUrl(DOMElement $element, $value) {
    switch ($element->tagName) {
      case 'a':
        $message = sprintf('Link with href="%s" not updated.', $value);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        break;

      case 'img':
        $message = sprintf('Image with src="%s" not updated.', $value);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        break;
    }
  }

  /**
   * Normalizes slashes in a path.
   *
   * Ensures the path starts and ends with exactly one slash.
   *
   * @param string $path
   *   The path.
   *
   * @return string
   *   The path, with slashes normalized.
   */
  protected function normalizePathSlashes($path) {
    $path = trim(trim($path), '/');
    $path = '/' . $path;
    if (strlen($path) > 1) {
      $path .= '/';
    }
    return $path;
  }

}
