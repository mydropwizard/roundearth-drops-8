<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Class SkipEquals.
 *
 * Skips the row when a value is equal.
 *
 * Options:
 *
 * - value: The value to check against. May be a singular value, or an array
 *     where all values are checked against.
 * - negate: Negate the check, skip when not equals (not implemented).
 * - strict: Use strict comparator (not implemented).
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_skip_equals"
 * )
 */
class SkipEquals extends Equals {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!$this->equals($value)) {
      throw new MigrateSkipRowException();
    }

    return $value;
  }

}
