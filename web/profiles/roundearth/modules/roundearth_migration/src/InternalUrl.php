<?php

namespace Drupal\roundearth_migration;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class InternalUrl.
 */
class InternalUrl {

  protected $hosts;

  /**
   * InternalUrl constructor.
   */
  public function __construct($hosts) {
    $this->hosts = $hosts;
  }

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @return static
   */
  public static function create(ConfigFactoryInterface $configFactory) {
    return new static($configFactory->get('roundearth_migration.settings')->get('hosts'));
  }

  /**
   * Gets the internal URL.
   *
   * @param string $url
   *   The URL.
   * @param array $hosts
   *   The hosts.
   * @param array $validSchemes
   *   List of valid schemes. URLs with schemes not specified as valid will not
   *   be handled as "internal". Defaults to 'http' and 'https'.
   *
   * @return string|null
   *   The internal URL, or NULL if not internal.
   */
  public function get($url, $hosts = NULL, array $validSchemes = ['http', 'https']) {
    $url = trim($url);

    // Skip empty links.
    if (strlen($url) < 1) {
      return NULL;
    }

    $parts = parse_url($url);

    if ($hosts === NULL) {
      $hosts = $this->hosts;
    }

    // If there's a host, make sure it's the right one.
    if (!empty($parts['host'])) {
      if (!in_array($parts['host'], $hosts)) {
        return NULL;
      }
    }

    // Only valid schemes.
    if (!empty($parts['scheme']) && !in_array($parts['scheme'], $validSchemes)) {
      return NULL;
    }

    // Skip processing on in-page anchors.
    if (substr($url, 0, 1) == '#') {
      return NULL;
    }

    // Ensure the path starts with a slash.
    $url = $parts['path'];
    if (substr($url, 0, 1) != '/') {
      $url = '/' . $url;
    }

    // Add the query and fragment.
    if (!empty($parts['query'])) {
      $url .= '?' . $parts['query'];
    }

    if (!empty($parts['fragment'])) {
      $url .= '#' . $parts['fragment'];
    }

    return $url;
  }

}
