(function ($, Drupal) {

  Drupal.behaviors.roundearthThemeToolbar = {
    attach: function (context) {
      const $body = $('body', context);
      const $toolbar = $('#toolbar-bar', context);
      const $civicrmMenu = $('#civicrm-menu', context);

      // If elements are not present, do nothing.
      if (! $body.length || !$toolbar.length || !$civicrmMenu.length) {
        return;
      }

      // Set the top spacing for the body.
      setBodyPadding();

      const $toolbarItem = $('.path-civicrm #toolbar-item-civicrm', context);
      const $toolbarTray = $('.toolbar-tray-horizontal', context);

      // Make the CiviCRM toolbar tab active to /civicrm routes.
      if ($toolbarItem.length) {
        setActiveTab($toolbarItem, true);
      }

      $(document).on('drupalToolbarTabChange.toolbar', function (model, tab) {
        const $tab = $(tab);
        setActiveTab($tab, $tab.data().toolbarTray === 'toolbar-item-civicrm-tray');
        setBodyPadding();
      });

      $(document).on('drupalToolbarOrientationChange.toolbar', function () {
        setBodyPadding();
      });

      // Show the dropdown on hover.
      $civicrmMenu.find('.menumain').on('mouseenter', function () {
        $(this).trigger('click');
      });

      // Sets the toolbar activeTab.
      function setActiveTab($element, isCiviCrmTab) {
        $toolbar.find('.toolbar-item.is-active').removeClass('is-active');
        $element.addClass('is-active');

        // Add a custom class based on the active item.
        $body.toggleClass('toolbar-civicrm', isCiviCrmTab);
      }

      function setBodyPadding() {
        // Recalculate toolbar height.
        var toolbarHeight = $toolbar.height();
        if ($civicrmMenu) {
          toolbarHeight += $civicrmMenu.height();
        }
        else if($toolbarTray.length) {
          toolbarHeight += $toolbarTray.height();
        }
        $body[0].style.setProperty('padding-top', toolbarHeight + 'px', 'important');
      }
    }
  };

})(jQuery, Drupal);
