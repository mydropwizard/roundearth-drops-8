/**
 * @file
 * Custom scripts for civicrm menu.
 */
(function (Drupal, $) {

  Drupal.behaviors.civicrmMenu = {
    attach: function (context) {
      // Add a custom class to body if the civicrm menu is loaded.
      var $civicrm = $('#civicrm-menu');
      
      if ($civicrm.length) {
        $civicrm.ready(function () {
          if ($civicrm.length) {
            $('body').addClass('has-civicrm-menu');

            $('#crm-qsearch').focusin(function () {
              $(this).addClass('focus');
            });
            $('#crm-qsearch').focusout(function () {
              $(this).removeClass('focus');
            });

            $('#sort_name_navigation').change(function () {
              $(this).parent().toggleClass('has-value', this.value.trim() !== '');
            });
          }
        });
      }
    }
  };

})(Drupal, jQuery);
