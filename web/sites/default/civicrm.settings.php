<?php

global $base_path, $base_url, $civicrm_root, $civicrm_setting;

// Settings to apply everywhere, override as needed.
$civicrm_setting['Developer Preferences']['environment'] = 'Development';
$civicrm_setting['Developer Preferences']['assetCache'] = '0';

/**
 * Pantheon settings.
 */
if (!empty($_SERVER['PRESSFLOW_SETTINGS'])) {
  $env = json_decode($_SERVER['PRESSFLOW_SETTINGS'], TRUE);
  if (!empty($env['conf']['pantheon_environment'])) {
    $pantheon_db = $env['databases']['default']['default'];
    $pantheon_conf = $env['conf'];
    $pantheon_root = $_ENV['HOME'] . ($_ENV['HOME'] !== '/' ? '/' : '');

    //user name and password
    $db_string = $pantheon_db['driver'] . '://' . $pantheon_db['username'] . ':' . $pantheon_db['password'] . '@';
    //host
    $db_string .= $pantheon_db['host'] . ':' . $pantheon_db['port'];
    // database
    $db_string .= '/' . $pantheon_db['database'] . '?new_link=true';

    // define the database strings
    define('CIVICRM_UF_DSN', $db_string);
    define('CIVICRM_DSN', $db_string);

    if ($pantheon_conf['pantheon_environment'] === 'lando') {
      $civicrm_root = $_SERVER['LANDO_MOUNT'] . '/vendor/civicrm/civicrm-core';
      define('CIVICRM_TEMPLATE_COMPILEDIR', $_SERVER['LANDO_WEBROOT'] . '/' . $pantheon_conf['file_private_path'] . '/civicrm/templates_c/');
    }
    else {
      $civicrm_root = $pantheon_root . '/code/vendor/civicrm/civicrm-core';
      define('CIVICRM_TEMPLATE_COMPILEDIR', $pantheon_root . '/files/private/civicrm/templates_c/');

      // For our custom hack to move just the templates_c directory to a special place.
      define('CIVICRM_REAL_TEMPLATE_COMPILEDIR', $_SERVER['PANTHEON_ROLLING_TMP'] . '/civicrm/templates_c/');
    }

    // Put the image upload URL in the public filesystem.
    $civicrm_setting['domain']['imageUploadDir'] = '[cms.root]/sites/default/files/civicrm/persist/contribute/';

    // Use Drupal base url and path
    define('CIVICRM_UF_BASEURL', $base_url . '/');
  }
}

/**
 * Local settings.
 */
if (file_exists(__DIR__ . '/civicrm.local.inc')) {
  require_once(__DIR__ . '/civicrm.local.inc');
}

/**
 * The upstream CiviCRM settings.
 */
require_once(__DIR__ . '/civicrm.settings.inc');

