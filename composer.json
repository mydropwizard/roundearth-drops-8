{
    "name": "roundearth/roundearth-composer-template",
    "description": "Template for composer-based RoundEarth projects.",
    "type": "project",
    "license": "GPL-2.0-or-later",
    "authors": [
        {
            "name": "myDropWizard Team",
            "role": "support@mydropwizard.com"
        }
    ],
    "repositories": {
        "drupal": {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        "drupal-ckeditor-plugins": {
            "type": "composer",
            "url": "https://panopoly.gitlab.io/drupal-ckeditor-plugins"
        },
        "asset-packagist": {
            "type": "composer",
            "url": "https://asset-packagist.org"
        },
        "roundearth": {
            "type": "vcs",
            "url": "https://gitlab.com/roundearth/roundearth.git"
        },
        "civicrm-core": {
            "type": "vcs",
            "url": "https://github.com/mydropwizard/civicrm-core.git"
        },
        "civicrm-drupal-8": {
            "type": "vcs",
            "url": "https://github.com/mydropwizard/civicrm-drupal-8.git"
        },
        "civicrm_entity": {
            "type": "vcs",
            "url": "https://github.com/mydropwizard/civicrm_entity.git"
        },
        "civicrm_group_roles": {
            "type": "vcs",
            "url": "https://github.com/mydropwizard/civicrm_group_roles.git"
        },
        "webform_civicrm": {
            "type": "vcs",
            "url": "https://github.com/colemanw/webform_civicrm.git"
        }
    },
    "require": {
        "php": ">=5.6",
        "civicrm/civicrm-core": "dev-roundearth-5.24.6 as 5.24.6",
        "civicrm/civicrm-drupal-8": "dev-roundearth-5.24.0 as dev-master",
        "composer/installers": "^1.2",
        "cweagans/composer-patches": "^1.6.5",
        "drupal-composer/drupal-scaffold": "^2.5",
        "drupal/civicrm_entity": "dev-roundearth as 3.x-dev",
        "drupal/console": "^1.0.2",
        "drupal/core": "~8.8.8",
        "drupal/video_embed_field": "dev-2.x",
        "drupal/webform_civicrm": "5.x-dev",
        "oomphinc/composer-installers-extender": "^1.1",
        "postal/postal": "^1.0",
        "roundearth/civicrm-composer-plugin": "*",
        "roundearth/roundearth": "dev-8.x-1.x as 1.x-dev",
        "vlucas/phpdotenv": "^2.4",
        "webflo/drupal-finder": "^1.0.0",
        "webmozart/path-util": "^2.3",
        "zaporylie/composer-drupal-optimizations": "^1.0"
    },
    "require-dev": {
        "webflo/drupal-core-require-dev": "~8.8.8"
    },
    "conflict": {
        "drupal/drupal": "*"
    },
    "replace": {
        "ckeditor/colorbutton": "*",
        "ckeditor/find": "*",
        "ckeditor/panelbutton": "*",
        "ckeditor/tabletoolstoolbar": "*",
        "ckeditor/tableresize": "*",
        "ckeditor/tableselection": "*",
        "ckeditor/texttransform": "*"
    },
    "minimum-stability": "dev",
    "prefer-stable": true,
    "config": {
        "sort-packages": true,
        "preferred-install": "dist",
        "autoloader-suffix": "Drupal8"
    },
    "autoload": {
        "classmap": [
            "scripts/composer/ScriptHandler.php",
            "scripts/roundearth/ScriptHandler.php"
        ],
        "files": ["load.environment.php"]
    },
    "scripts": {
        "pre-install-cmd": [
            "DrupalProject\\composer\\ScriptHandler::checkComposerVersion"
        ],
        "pre-update-cmd": [
            "DrupalProject\\composer\\ScriptHandler::checkComposerVersion"
        ],
        "post-install-cmd": [
            "DrupalProject\\composer\\ScriptHandler::createRequiredFiles",
            "DrupalProject\\roundearth\\ScriptHandler::buildPangeaTheme",
            "DrupalProject\\roundearth\\ScriptHandler::getVersafixTemplate",
            "@composer run-script clean-git"
        ],
        "post-update-cmd": [
            "DrupalProject\\composer\\ScriptHandler::createRequiredFiles",
            "DrupalProject\\roundearth\\ScriptHandler::buildPangeaTheme",
            "DrupalProject\\roundearth\\ScriptHandler::getVersafixTemplate",
            "@composer run-script clean-git"
        ],
        "clean-git": [
            "find -mindepth 2 -name .git -prune -exec rm -rf \\{\\} \\;",
            "find -mindepth 2 -name .gitignore -exec rm -f \\{\\} \\;"
        ],
        "build-pangea-theme": [
            "DrupalProject\\roundearth\\ScriptHandler::buildPangeaTheme"
        ],
        "get-versafix-template": [
            "DrupalProject\\roundearth\\ScriptHandler::getVersafixTemplate"
        ]
    },
    "extra": {
        "installer-types": [
            "bower-asset",
            "npm-asset"
        ],
        "installer-paths": {
            "web/core": ["type:drupal-core"],
            "web/libraries/{$name}": [
                "type:drupal-library",
                "type:bower-asset",
                "type:npm-asset"
            ],
            "web/modules/contrib/{$name}": ["type:drupal-module"],
            "web/profiles/{$name}": ["type:drupal-profile"],
            "web/themes/{$name}": ["type:drupal-theme"],
            "drush/Commands/{$name}": ["type:drupal-drush"]
        },
        "drupal-scaffold": {
            "initial": {
                ".editorconfig": "../.editorconfig",
                ".gitattributes": "../.gitattributes"
            }
        },
        "enable-patching": true,
        "composer-exit-on-patch-failure": true,
        "patchLevel": {
            "drupal/core": "-p2"
        },
        "patches": {
            "drupal/core": {
                "Forum term canonical links": "https://www.drupal.org/files/issues/2010132-104.patch",
                "Image Formatter Links When No Access (waiting for merge in 8.8?)": "https://www.drupal.org/files/issues/2019-05-02/drupal-image_formatter_links-2999235-7.patch",
                "Filtering incorrectly on 'Summary or Trimmed' fields": "https://www.drupal.org/files/issues/2019-12-10/drupal-core-2922569-9.patch"
            }
        },
        "civicrm": {
            "extensions": {
                "org.civicrm.shoreditch": "https://github.com/civicrm/org.civicrm.shoreditch/archive/v0.1-alpha30.zip",
                "org.civicrm.flexmailer": {
                  "url": "https://github.com/civicrm/org.civicrm.flexmailer/archive/v1.0-beta1.zip",
                  "patches": [
                    "./patches/flexmailer.patch"
                  ]
                },
                "uk.co.vedaconsulting.mosaico": "https://storage.googleapis.com/civicrm/mosaico/2.0-beta4.1528762072/uk.co.vedaconsulting.mosaico-2.0-beta4.1528762072.zip",
                "org.civicrm.doctorwhen": "https://github.com/civicrm/org.civicrm.doctorwhen/archive/master.zip",
                "com.iatspayments.civicrm": "https://github.com/iATSPayments/com.iatspayments.civicrm/archive/1.7.0.zip",
                "uk.squiffle.kam": "https://github.com/aydun/uk.squiffle.kam/archive/1.1.zip",
                "io.roundearth.postal": "https://gitlab.com/roundearth/io.roundearth.postal/-/archive/master/io.roundearth.postal-master.zip",
                "io.roundearth.mailgun": "https://gitlab.com/roundearth/io.roundearth.mailgun/-/archive/master/io.roundearth.mailgun-master.zip"
            }
        }
    }
}
