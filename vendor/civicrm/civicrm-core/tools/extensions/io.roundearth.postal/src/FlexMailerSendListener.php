<?php

namespace Civi\Postal;

use Civi\FlexMailer\Event\SendBatchEvent;
use Postal\SendMessage;

/**
 * Listens for FlexMailer send events and sends messages via Postal.
 */
class FlexMailerSendListener {

  /**
   * @var \Civi\Postal\PostalAPI
   */
  protected $api;

  /**
   * Constructs a FlexMailerSendListener.
   *
   * @param \Civi\Postal\PostalAPI $api
   *   The Postal API service.
   */
  public function __construct(PostalAPI $api) {
    $this->api = $api;
  }

  /**
   * Process a FlexMailer send event.
   *
   * @param \Civi\FlexMailer\Event\SendBatchEvent $event
   */
  public function processEvent(SendBatchEvent $event) {
    try {
      $client = $this->api->getClient();
    }
    catch (\Exception $e) {
      // If we can't get a valid API client, then don't do anything.
      return;
    }

    // Stop normal delivery.
    $event->stopPropagation();

    $job = $event->getJob();
    $mailing = $event->getMailing();
    $job_date = \CRM_Utils_Date::isoToMysql($job->scheduled_date);

    $targetParams = $deliveredParams = [];
    $attachment_cache = [];

    /** @var \Civi\FlexMailer\FlexMailerTask $task */
    foreach ($event->getTasks() as $task) {
      if (!$task->hasContent()) {
        continue;
      }

      list ($params, $headers) = $this->extractParameters($task->getMailParams());

      $message = new SendMessage($client);

      $message->to("{$params['toname']} <{$params['toemail']}>");
      $message->from("{$mailing->from_name} <{$mailing->from_email}>");
      $message->subject($params['subject']);
      $message->plainBody($params['text']);
      $message->htmlBody($params['html']);

      if (!empty($params['cc'])) {
        $message->cc($params['cc']);
      }

      if (!empty($params['bcc'])) {
        $message->bcc($params['bcc']);
      }

      foreach ($headers as $header => $value) {
        $message->header($header, $value);
      }

      foreach ($params['attachments'] as $attachment) {
        // Get the actual attachment data.
        if (isset($attachment_cache[$attachment['fullPath']])) {
          $attachment_data = $attachment_cache[$attachment['fullPath']];
        }
        else {
          $attachment_data = $attachment_cache[$attachment['fullPath']] = file_get_contents($attachment['fullPath']);
        }

        $message->attach($attachment['fileName'], $attachment['mime_type'], $attachment_data);
      }

      $message->send();

      // Keep track of parameters for delivery event.
      $deliveredParams[] = $task->getEventQueueId();
      $targetParams[] = $task->getContactId();
   }

    // Record the delivery event.
    $completed = $job->writeToDB(
      $deliveredParams,
      $targetParams,
      $mailing,
      $job_date
    );

    $event->setCompleted($completed);
  }

  /**
   * Extracts the known parameters and headers from parameters array.
   *
   * @param array $params
   *   The parameters array from CiviCRM.
   *
   * @return array
   *   An array with two arrays: the first is the known parameters, and the
   *   second is the headers.
   */
  protected function extractParameters($params) {
    $params = array_change_key_case($params, CASE_LOWER);

    $known_param_names = [
      'subject',
      'text',
      'html',
      'job_id',
      'from',
      'toemail',
      'toname',
      'attachments',
    ];

    $known_params = [];
    foreach ($known_param_names as $key) {
      $known_params[$key] = $params[$key];
      unset($params[$key]);
    }

    return [$known_params, $params];
  }

}
