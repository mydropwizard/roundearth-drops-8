<?php

namespace Civi\Postal;

/**
 * Factory service for getting Postal client objects.
 */
class PostalAPI {

  /**
   * Get an API client object.
   *
   * @return \Postal\Client
   *   A client object.
   *
   * @throws \Exception
   *   If unable to create client object.
   */
  public function getClient() {
    if (!class_exists('Postal\Client')) {
      throw new \Exception('You need to install the Postal client via "composer require postal/postal"');
    }

    $postal_url = \Civi::settings()->get('postal_url');
    $postal_api_key = \Civi::settings()->get('postal_api_key');

    if (empty($postal_url) || empty($postal_api_key)) {
      throw new \Exception('You must set the Postal URL and API Key at /civicrm/postal/settings');
    }

    // Remove trailing slash, because it messes up the API.
    $postal_url = rtrim($postal_url, '/');

    return new \Postal\Client($postal_url, $postal_api_key);
  }

}