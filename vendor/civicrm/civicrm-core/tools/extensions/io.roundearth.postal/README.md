# io.roundearth.postal

![Screenshot](/images/screenshot.png)

Provides CiviCRM integration with [Postal](https://github.com/atech/postal),
the Open Source mail delivery platform.

Postal is similar to commercial services like Sendgrid, Postmark or Mandrill,
which are platforms for incoming and outgoing e-mail via an API (or SMTP) and
webhooks. However, it's Open Source and you run it on your own server.

To support outgoing e-mail, you can configure CiviCRM to send via SMTP, and
just receive information about bounces and clicks from Postal via webhooks.
The goal is to eventually support outgoing e-mail via the
[FlexiMailer](https://docs.civicrm.org/flexmailer/en/latest/develop/)
extension and the Postal API, but for expediency, we're just going to rely
on SMTP for now.

In order to access the Postal API (needed by the CiviMail integration),
you'll need to install the PHP library via composer:

```$bash
composer require postal/postal

```

We also plan to support a special 'E-mail to activity' webhook which can
receive data from an inbound Postal mailbox.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v5.4+
* CiviCRM 4.7+

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl io.roundearth.postal@https://gitlab.com/roundearth/io.roundearth.postal/repository/archive.zip?ref=master
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://gitlab.com/roundearth/io.roundearth.postal.git
cv en postal
```

## Usage

(* FIXME: Where would a new user navigate to get started? What changes would they see? *)

## Known Issues

(* FIXME *)

## TODO

- Send via the Postal API (with Fleximailer)
- Optionally disable CiviMail's open and click tracking and use the webhook
  from Postal
- Only add the CiviMailPostalWebhookListener if CiviMail is enabled
- Add a /civicrm/postal/bcc page to do 'Email-to-activity' processing from a
  Postal inbox
- Include custom client for Postal API so we don't have to depend on 
  postal/postal (which kinda sucks and needs to be install via composer)
- Document how to setup webhooks and configure better
- Add some kind of validation on the settings form
- Only try to do stuff if properly configured (and complain when not)

